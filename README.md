# IP Fabric Integration with Microsoft Power Query

This integration allows you to connect IP Fabric to Microsoft Excel or Power BI
using [Microsoft's Power Query M language](
https://learn.microsoft.com/en-us/powerquery-m/).

## Getting started

The easiest way to get started is using the Excel or Power BI template
located under [templates](templates).  These files require minimal editing 
for connecting to your IP Fabric instance.
An example file is also available, once configured with your IP Fabric
instance, it will generate a report with device information, including
stack members and SNMP location.

**NOTE: Power BI does not support TLSv1.3 at this time (May 2024).  Please reenable this
feature by following [Custom TLS](https://docs.ipfabric.io/latest/System_Administration/Command_Line_Interface/Custom_TLS/).**

## References

For more detailed information please take a look at:

- Blog Post: [Integrating IP Fabric into Excel & Power BI](https://ipfabric.io/blog/microsoft-excel-powerbi/)
- YouTube Videos:
  - [Integrating IP Fabric with Excel & Power BI | Part 1: Excel](https://youtu.be/sDjcyoe_lhk)
  - [Integrating IP Fabric with Excel and Power BI | Part 2: Power BI](https://youtu.be/jT0lWwPd7qc)

## API Tokens

You must create an [API token](
https://docs.ipfabric.io/main/IP_Fabric_Settings/api_tokens/)
to use within the file.  This token needs to be able to:

- Read tables that you are going to query
  - i.e. (POST) `/tables/inventory/devices`
- Read (POST) `/tables/management/snapshots`
  - To populate the "Snapshots" data source table
- Read (GET) `/reports`

## Authentication

***Sharing files or publishing the Power BI data source is not recommended as 
your personal API token will be saved
inside the documents.  This allows other users to copy the token and interact
with IPF as yourself.***

If using a shared file it is recommended to perform one of the following before 
sharing:

- Edit the `IPF_TOKEN` variable and change it to any string prior to sending.
- Create a Read Only User/Token:
  - Create a service account for IP Fabric with Read Only permissions
    - See [RBAC documentation](
    https://docs.ipfabric.io/latest/IP_Fabric_Settings/user_management/)
  - Create an API Token for the user
  - Use the API token in your Excel or Power BI files.

## Timestamps

Timestamp columns in IP Fabric are stored as epoch time in milliseconds
(example: End of X dates).  Duration columns are stored in seconds 
(examples: device uptime or routing protocol session age).

There is a helper function within `queryIPF` function that will converter these 
types of data for you into the following:

- Epoch timestamps:
  - Converted to `datetime` object
  - New column named `originalColName + AsDate` (`endSale` -> `endSaleAsDate`)
- Duration columns:
  - Converted to `duration` object (DD:HH:MM:SS)
  - New column named `originalColName + AsTime` (`uptime` -> `uptimeAsTime`)

Original invoked function:

```text
let
    Source = queryIPF("https://demo1.us.ipfabric.io/api/v6.0/tables/inventory/devices", null, null, null)
in
    Source
```

Edited function for a column with `duration`:

```text
let
    Source = queryIPF("https://demo1.us.ipfabric.io/api/v6.0/tables/inventory/devices", null, null, {"uptime"})
in
    Source
```

Edited function for a column with `timestamp`:

```text
let
    Source = queryIPF("https://demo1.us.ipfabric.io/api/v6.0/tables/reports/eof/summary", null, {"endSale", "endMaintenance", "endSupport"}, null)
in
    Source
```

If you set `reports=true` and the column with either timestamp or duration has 
an intent rule configured on it; then the query will automatically account for 
this and apply the conversion to the expanded record column:

```text
let
    Source = queryIPF("https://demo1.us.ipfabric.io/api/v6.0/tables/inventory/devices", null, null, {"uptime"})
in
    Source

// New column will be "uptimeAsTime" as there is an Intent check on the "uptime" column.
```

**Please note that Microsoft Power Query uses curly braces for 
lists `{"item1", "item2"}` and square brackets for records (dict/object) 
`[key="value"]`.**

## Intent Checks - (Optional) reports

If you would like to include intent data simply set `reports=true` in the 
`queryIPF` function.  When reports are pulled the columns with intent checks
return a JSON object like `{"data": "originalValue", "severity": int}`

Severity can be one of the following:

- -1: No color
-  0: Green
- 10: Blue
- 20: Amber
- 30: Red

The Power Query will have expanded these into new columns for instance the 
"uptime" column in Inventory > Devices table has an Intent Rule.  When reports
is set to true instead of a column named `uptime` you will have two columns
named `uptime` and `uptime.severity`.

```text
let
    Source = queryIPF("https://demo1.us.ipfabric.io/api/v6.0/tables/inventory/devices", true, null, null)
in
    Source
```

## Expanding "Record" Columns

If the data returned is a JSON object the table will show `[Record]` instead 
of the data.  This occurs when you set `reports=true` when invoking
the `queryIPF` function (these columns are automatically expanded) but 
can also occur naturally as some tables' columns are nested JSON.

These records can be expanded to show the nested object by following the 
instructions under 
[Expand a Record structured column](https://support.microsoft.com/en-us/office/work-with-a-list-record-or-table-structured-column-power-query-d5e552be-c143-4f06-9a5e-0960bbaaf480).

Ensure you check `Use original column name as prefix` because unselecting this
will make a column named `data` instead of `originalColName.data` making it 
hard to distinguish what the original column was.

## Expanding "List" Columns

These records can be expanded to show all data by following the instructions
under
[Expand a List structured column](https://support.microsoft.com/en-us/office/work-with-a-list-record-or-table-structured-column-power-query-d5e552be-c143-4f06-9a5e-0960bbaaf480).

After this expansion if the list contained 2 items then the table will now have
two rows instead of one.  Example: NTP Summary table "Sources" column is a list
of sources:

```json
{
    "sn": "3A69CA14",
    "hostname": "SITE4",
    "siteName": "SDWAN",
    "sources": [
        {
            "source": "10.0.10.10",
            "sync": true
        },
        {
            "source": "217.31.202.100",
            "sync": true
        }
    ]
}
```

How Microsoft will show this without expanding:

| sn       | hostname | siteName | sources  |
|----------|----------|----------|----------|
| 3A69CA14 | SITE4    | SDWAN    | \[List\] |

After expanding the list:

| sn       | hostname | siteName | sources    |
|----------|----------|----------|------------|
| 3A69CA14 | SITE4    | SDWAN    | \[Record\] |
| 3A69CA14 | SITE4    | SDWAN    | \[Record\] |

After expanding the record:

| sn       | hostname | siteName | sources.source | sources.sync |
|----------|----------|----------|----------------|--------------|
| 3A69CA14 | SITE4    | SDWAN    | 10.0.10.10     | true         |
| 3A69CA14 | SITE4    | SDWAN    | 217.31.202.100 | true         |

## SSL Certificates

The computer running the Power Query must trust the SSL certificate your IP
Fabric instance is using.  This can be accomplished two different ways.

Option 1: [Install a signed certificate on the IPF Server](
https://docs.ipfabric.io/latest/IP_Fabric_Settings/advanced/ipf_cert/) (The
cert must be signed by a trusted certification authority).

Option 2: Install the certificate to `Trusted Root Certification Authorities`:

This is required if using a self-signed certificate or the signed certificate's
issuer is not trusted by your machine.

*Please note that the CN in the certificate must reference either the IP or FQDN
of the server.  You cannot use the default certificate that is installed.*

To create a self-signed cert with the correct CN run the below commands and
replace `<IP OR FQDN>` with your information. (example: `"/CN=192.168.0.1"`
and in your queries you must use `https://192.168.0.1` as the URL)

```shell
root@ipfabric:~# openssl req -nodes -x509 -newkey rsa:4096 -sha256 -days 3650 \
    -keyout /etc/nginx/ssl/server.key \
    -out /etc/nginx/ssl/server.crt \
    -subj "/CN=<IP OR FQDN>"
root@ipfabric:~# systemctl restart nginx
```

Then install the certificate:

- Get the CRT file on the machine working with the connector:
  - Copy `/etc/nginx/ssl/server.crt` to your local device
  - Reload the IP Fabric GUI in a browser and export the certificate.
- Double-click the CRT file.
- Click "Install Certificate"
- Select "Local Machine" (requires admin privileges)
- Select "Place all certificates in the following store"
- Browse and select `Trusted Root Certification Authorities`
- Click "Next" and "Finish"
